package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/prysmakou/gora/config"
)

func index(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Method)
	log.Println(r.URL)
	log.Println(r.Proto)
	log.Println(r.Host)
	log.Println(r.RemoteAddr)
	log.Println(r.RequestURI)
	for k, v := range r.Header {
		log.Println("key:", k, "value:", v)
	}
	fmt.Fprintf(w, "REQ:")
}

var configFile string

func main() {
	log.Println("Starting")
	flag.StringVar(&configFile, "c", "", "Configuration File")
	flag.Parse()
	c := config.InitConfigFromFile(configFile)
	log.Println(c.Key)
	http.HandleFunc("/", index)
	err := http.ListenAndServe(getPort(), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func getPort() string {
	configuredPort := ":3000"
	if os.Getenv("PORT") != "" {
		return ":" + os.Getenv("PORT")
	}
	return configuredPort
}
