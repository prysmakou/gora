package config

import (
	"io/ioutil"

	"github.com/cloudfoundry-incubator/candiedyaml"
)

type Config struct {
	Key string `yaml:"key"`
}

var defaultConfig = Config{
	Key: "default",
}

func (c *Config) Initialize(configYAML []byte) error {
	return candiedyaml.Unmarshal(configYAML, &c)
}
func DefaultConfig() *Config {
	c := defaultConfig
	return &c
}

func InitConfigFromFile(path string) *Config {
	var c = DefaultConfig()
	b, e := ioutil.ReadFile(path)
	if e != nil {
		panic(e.Error())
	}
	e = c.Initialize(b)
	if e != nil {
		panic(e.Error())
	}
	return c
}
